<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
        return view('cast', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
        ]);

            $cast = new Cast;
            
            $cast->nama = $request->nama;
            $cast->umur = $request->umur;
            $cast->bio = $request->bio;

            $cast->save();
        
        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data = Cast::where('id', $id)->first();
        return view('show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::where('id', $id)->first();
        return view('edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cast::where('id', $id)
                ->update([
                    'nama' => $request->nama,
                    'umur' => $request->umur,
                    'bio' => $request->bio,
                    
                ]);
                return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = Cast::where('id', $id)->delete();
        return redirect('/cast');
    }
}
