@extends('layout.master')
@section('title')
    Show Cast
@endsection

@section('content')

<div class="card" >
    <div class="card-header" >
      Cast
    </div>
    <div class="card-body">
      <h5 class="card-title">{{$data->nama}}</h5>
      <p class="card-text text-danger">Umur: {{$data->umur}}</p>
      <p class="card-text">{{$data->bio}}</p>
      
    </div>
  </div>


@endsection