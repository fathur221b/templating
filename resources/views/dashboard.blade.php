@extends('layout.master')
@section('title')
    Halaman Dashboard
@endsection

@section('content')

<div class="row">
    <div class="col align-items-start">
        <div class="card" style="width: 18rem;">
            <img src="img/table.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Halaman Table</h5>
              <p class="card-text">Berisi tentang table proses  dengan tampilan yang keren</p>
              <a href="/table" class="btn btn-primary">Lihat Table</a>
            </div>
          </div>
    </div>
    <div class="col align-items-center">
        <div class="card" style="width: 18rem;">
            <img src="img/data-table.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Halaman Data-Table</h5>
              <p class="card-text">Berisi tentang Data-table yang dilengkapi dengan fitur pencarian dan paging</p>
              <a href="/data-table" class="btn btn-primary">Lihat Table</a>
            </div>
          </div>
    </div>
</div>


@endsection